﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoPrototypePattern
{
    public interface IEmployee
    {
        // Method for cloning
        IEmployee Clone();
    }
 
    public class PermanentEmployee : IEmployee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string EmploymentType { get; set; }

        public IEmployee Clone()
        {
            return this.MemberwiseClone() as IEmployee;
        }
    }

    public class TemporaryEmployee : IEmployee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string EmploymentType { get; set; }

       
        public IEmployee Clone()
        {
            return this.MemberwiseClone() as IEmployee;
        }
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            PermanentEmployee permanentEmployee = new PermanentEmployee();
            permanentEmployee.Name = "Sam";
            permanentEmployee.Age = 25;
            permanentEmployee.EmploymentType = "Permanent";

           
            PermanentEmployee permanentEmployeeClone = (PermanentEmployee)permanentEmployee.Clone();
            permanentEmployeeClone.Name = "Tom";
            permanentEmployeeClone.Age = 30;

            Console.WriteLine("Permanent Employee Details");
            Console.WriteLine("Name: {0}; Age: {1}; Employment Type: {2}", permanentEmployee.Name, permanentEmployee.Age, permanentEmployee.EmploymentType);

            Console.WriteLine("Cloned Permanent Employee Details");
            Console.WriteLine("Name: {0}; Age: {1}; Employment Type: {2}", permanentEmployeeClone.Name, permanentEmployeeClone.Age, permanentEmployeeClone.EmploymentType);

         

            Console.ReadLine();
        }
       
    }
 
}
