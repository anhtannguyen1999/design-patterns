﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
    public class PersonFacade
    {
        Name name;
        Address address;
        PhoneNumber phoneNumber;
        public PersonFacade()
        {

        }

        public PersonFacade(Name name, Address address, PhoneNumber phoneNumber)
        {

            this.name = name;

            this.address = address;

            this.phoneNumber = phoneNumber;

        }
        public Name getName()
        {

            return name;

        }

        public void setName(Name name)
        {

            this.name = name;

        }
        public Address getAddress()
        {

            return address;

        }

        public void setAddress(Address address)
        {

            this.address = address;

        }
        public PhoneNumber getPhoneNumber()
        {

            return phoneNumber;

        }

        public void setPhoneNumber(PhoneNumber phoneNumber)
        {

            this.phoneNumber = phoneNumber;

        }
        public void display()
        {
            this.name.display();
            this.address.display();
            this.phoneNumber.display();
            Console.WriteLine("---------------------------");
        }
        public void setInfomation()
        {
            Console.WriteLine("Nhap duong:");
            String street = Console.ReadLine();
            Console.WriteLine("Nhap quan huyen:");
            String district = Console.ReadLine();
            Console.WriteLine("Nhap tinh/TP:");
            String city = Console.ReadLine();
            Console.WriteLine("Nhap ho:");
            String firstName = Console.ReadLine();
            Console.WriteLine("Nhap dem:");
            String middleName = Console.ReadLine();
            Console.WriteLine("Nhap ten:");
            String lastName = Console.ReadLine();
            Console.WriteLine("Nhap so dien thoai:");
            String phoneNumber = Console.ReadLine();
            this.phoneNumber = new PhoneNumber(phoneNumber);
            this.address = new Address(street, district, city);
            this.name = new Name(firstName, middleName, lastName);
        }
    }
}

