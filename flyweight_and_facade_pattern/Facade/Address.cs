﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
    public class Address
    {
        String city;
        String street;
        String district;
        public Address()
        {
        }
        public Address(String city, String district, String street)
        {
            this.city = city;
            this.street = street;
            this.district = district;
        }
        public String getCity()
        {
            return city;
        }
        public void setCity(String city)
        {
            this.city = city;
        }
        public String getStreet()
        {
            return street;
        }
        public void setStreet(String street)
        {
            this.street = street;
        }
        public String getDistrict()
        {
            return district;
        }
        public void setDistrict(String district)
        {
            this.district = district;
        }
        public void display()
        {
            Console.WriteLine("Address: " + this.street + "-" + this.district + "-" + this.city );
        }
    }
}
