﻿using System;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonFacade person = new PersonFacade();
            person.setInfomation();
            person.display();
        }
    }
}
