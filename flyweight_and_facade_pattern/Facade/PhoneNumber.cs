﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
    public class PhoneNumber
    {
        String phoneNumber;
        public PhoneNumber()
        {
        }
        public PhoneNumber(String phongNumber)
        {
            this.phoneNumber = phongNumber;
        }
        public String getPhoneNumber()
        {
            return phoneNumber;
        }
        public void setPhoneNumber(String phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }
        public void display()
        {
            Console.WriteLine("PhoneNumber: " + " " + this.phoneNumber);
        }
    }
}
