﻿using FlyweightPattern.FlyweightFactory;
using System;

namespace FlyweightPattern
{
    class Program
    {
        static void Main(string[] args)
        {                       
            Game game = new Game();
            game.Update("Down");
            while(true)
            {
                var key = Console.ReadKey().Key;
                switch(key)
                {
                    case ConsoleKey.Enter:
                        game.AddBom();
                        break;
                    case ConsoleKey.UpArrow:
                        game.Update("Up");
                        break;
                    case ConsoleKey.DownArrow:
                        game.Update("Down");
                        break;
                    case ConsoleKey.LeftArrow:
                        game.Update("Left");
                        break;
                    case ConsoleKey.RightArrow:
                        game.Update("Right");
                        break;                       
                    default:
                        return;
                }
                
            }
        }

        
    }
}
