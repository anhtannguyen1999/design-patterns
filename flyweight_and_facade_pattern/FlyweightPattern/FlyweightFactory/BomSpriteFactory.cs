﻿using FlyweightPattern.ConcreteFlyweight;
using FlyweightPattern.Flyweight;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlyweightPattern.FlyweightFactory
{
    public class BomSpriteFactory
    {
        private static BomSpriteFactory instance = null;
        private Dictionary<string,BomSprite> BomSprites = new Dictionary<string, BomSprite>();
        
        private BomSpriteFactory()
        {

        }
        public static BomSpriteFactory GetInstance()
        {
            if(instance==null)
            {
                instance = new BomSpriteFactory();               
            }
            return instance;
        }
        public BomSprite GetBomSprite(string key)
        {
            BomSprite Result = null;
            
            if (BomSprites.ContainsKey(key))
                Result = BomSprites[key];
            else
            {
                switch(key)
                {
                    case "Small":
                        BomSprites.Add("Small",new SmallBomSprite());
                        Game.RAMcost += 1000;
                        break;
                    case "Big":
                        BomSprites.Add("Big", new BigBomSprite());
                        Game.RAMcost += 3000;
                        break;
                    default:
                        break;
                }
                Result=BomSprites[key];
            }
            return Result;
        }
        public int GetSize()
        {
            return BomSprites.Count();
        }

    }
}
