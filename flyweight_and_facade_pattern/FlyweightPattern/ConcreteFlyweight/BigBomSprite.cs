﻿using FlyweightPattern.Flyweight;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern.ConcreteFlyweight
{
    public class BigBomSprite: BomSprite
    {
        public BigBomSprite()
        {
            Image = "\x2593";
        }
        public override void Render(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.WriteLine(Image);
        }
    }
}
