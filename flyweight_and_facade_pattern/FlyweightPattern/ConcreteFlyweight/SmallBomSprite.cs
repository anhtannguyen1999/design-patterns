﻿using FlyweightPattern.Flyweight;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern.ConcreteFlyweight
{
    public class SmallBomSprite :BomSprite
    {
        public SmallBomSprite()
        {
            Image = "\x263C";
        }

        public override void Render(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.WriteLine(Image);
        }
    }
}
