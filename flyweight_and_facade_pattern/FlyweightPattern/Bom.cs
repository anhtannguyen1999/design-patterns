﻿using FlyweightPattern.FlyweightFactory;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern
{
    public class Bom
    {
        int X;
        int Y;
        float Weight;
        public Bom(int x,int y)
        {
            X = x;
            Y = y;
            Random r = new Random();
            Weight =(float)r.Next(1, 10);
            Game.RAMcost += 12;
        }
        public void RenderBom()
        {
            BomSpriteFactory bomSpriteFactory = BomSpriteFactory.GetInstance();
            if (Weight < 5)
            {
                Console.SetCursorPosition(X, Y);
                bomSpriteFactory.GetBomSprite("Small").Render(X, Y);              
            }
                
            else
            {
                Console.SetCursorPosition(X, Y);
                bomSpriteFactory.GetBomSprite("Big").Render(X, Y);               
            }
                
        }
    }
}
