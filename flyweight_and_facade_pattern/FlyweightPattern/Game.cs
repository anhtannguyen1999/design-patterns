﻿using FlyweightPattern.FlyweightFactory;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern
{
    public class Game
    {
        List<Bom> Boms = new List<Bom>();
        public static int RAMcost = 0;
        int X=0;
        int Y=5;
        int Speed = 2;
        public void Update(string key)
        {
            switch(key)
            {
                case "Up":
                    Y-=Speed;
                    break;
                case "Down":
                    Y+=Speed;
                    break;
                case "Left":
                    X-=Speed;
                    break;
                case "Right":
                    X+=Speed;
                    break;
                default:
                    break;
            }
            Console.Clear();
            Console.WriteLine("Demo Flyweight Design Pattern ");
            Console.WriteLine("Number of Boms: " + Boms.Count);
            Console.WriteLine("Number of BomSpriteFlyweights in Factory: " + BomSpriteFactory.GetInstance().GetSize().ToString());
            Console.WriteLine($"RAM Cost:{Game.RAMcost} KB ");
            Console.WriteLine($"X:{X}\nY:{Y}");
            Console.WriteLine("---------------------------------------------------------------");
            Console.SetCursorPosition(X, Y);
            Console.Write("<\x263a>");
            foreach (var b in Boms)
            {
                b.RenderBom();
            }
        }
        public void AddBom()
        {
            Boms.Add(new Bom(X, Y));
            Update("");
        }

    }
}
