﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern.Flyweight
{
    public abstract class BomSprite
    {
        protected string Image;

        public abstract void Render(int x, int y) ;

    }
}
