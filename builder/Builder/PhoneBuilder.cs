﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    public abstract class PhoneBuilder
    {
        protected MobilePhone phone;

        public MobilePhone Phone 
        { 
            get { return phone; }
        }

        //abstract build method
        public abstract void BuildScreen();
        public abstract void BuildBattery();
        public abstract void BuildOS();
        public abstract void BuildStylus();
        
    }
}
