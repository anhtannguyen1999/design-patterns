﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    public class IOSPhoneBuilder: PhoneBuilder
    {
        public IOSPhoneBuilder()
        {
            phone = new MobilePhone("IOS Phone");
        }
        public override void BuildBattery()
        {
            phone.PhoneBattery = Battery.MAH_3000;
        }

        public override void BuildOS()
        {
            phone.PhoneOS = OperatingSystem.IOS;
        }

        public override void BuildScreen()
        {
            phone.PhoneScreen = ScreenType.ScreenType_TOUCH_CAPACITIVE;
        }

        public override void BuildStylus()
        {
            phone.PhoneStylus = Stylus.NO;
        }
    }
}
