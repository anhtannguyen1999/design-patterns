﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    public class AndroidPhoneBuilder : PhoneBuilder
    {
        public AndroidPhoneBuilder()
        {
            phone = new MobilePhone("Android Phone");
        }
        public override void BuildBattery()
        {
            phone.PhoneBattery = Battery.MAH_1500;
        }

        public override void BuildOS()
        {
            phone.PhoneOS = OperatingSystem.ANDROID;
        }

        public override void BuildScreen()
        {
            phone.PhoneScreen = ScreenType.ScreenType_TOUCH_RESISTIVE;
        }

        public override void BuildStylus()
        {
            phone.PhoneStylus = Stylus.YES;
        }
    }
}
