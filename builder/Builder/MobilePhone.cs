﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    //Product class
    public class MobilePhone
    {
        private string name;
        private ScreenType phoneScreen;
        private Battery phoneBattery;
        private OperatingSystem phoneOS;
        private Stylus phoneStylus;
        public MobilePhone(string name)
        {
            this.name = name;
        }

        public string PhoneName
        {
            get { return name; }
        }

        public ScreenType PhoneScreen
        {
            get { return phoneScreen; }
            set { phoneScreen = value; }
        }

        public Battery PhoneBattery
        {
            get { return phoneBattery; }
            set { phoneBattery = value; }
        }

        public OperatingSystem PhoneOS
        {
            get { return phoneOS; }
            set { phoneOS = value; }
        }

        public Stylus PhoneStylus
        {
            get { return phoneStylus; }
            set { phoneStylus = value; }
        }

        public override string ToString()
        {
            return string.Format("Name: {0}\nScreen: {1}\nBattery {2}\nOS: {3}\nStylus: {4}",
                PhoneName, PhoneScreen, PhoneBattery, PhoneOS, PhoneStylus);
        }
    }

    // Cac Enum de dinh nghia cac phan cua dien thoai
    public enum ScreenType
    {
        ScreenType_TOUCH_CAPACITIVE,
        ScreenType_TOUCH_RESISTIVE,
        ScreenType_NON_TOUCH
    };

    public enum Battery
    {
        MAH_1000,
        MAH_1500,
        MAH_2000,
        MAH_3000
    };

    public enum OperatingSystem
    {
        ANDROID,
        IOS,
        WINDOWS_PHONE,
        SYMBIAN
    };

    public enum Stylus
    {
        YES,
        NO
    };
}
