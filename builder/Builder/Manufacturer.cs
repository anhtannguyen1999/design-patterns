﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    public class Manufacturer
    {
        public void Construct(PhoneBuilder phoneBuilder)
        {
            phoneBuilder.BuildBattery();
            phoneBuilder.BuildOS();
            phoneBuilder.BuildScreen();
            phoneBuilder.BuildStylus();
        }
    }
}
