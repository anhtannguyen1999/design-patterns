﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace iterator
{
    class Collection : IAbstractCollection
    {
        private ArrayList _items = new ArrayList();

        public Iterator CreateIterator()
        {
            return new Iterator(this);
        }

        // Gets item count

        public int Count
        {
            get { return _items.Count; }
        }

        public object GetItem(int index)
        {
            return _items[index];
        }

        public void AddItem(Item value)
        {
            _items.Add(value);
        }
    }
}
