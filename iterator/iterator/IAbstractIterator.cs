﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iterator
{
    interface IAbstractIterator
    {
        Item First();
        Item Next();
        bool HasNext();
    }
}
