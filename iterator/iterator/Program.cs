﻿using System;

namespace iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            Collection collection = new Collection();
            collection.AddItem(new Item("Item 0"));
            collection.AddItem(new Item("Item 1"));
            collection.AddItem(new Item("Item 2"));
            collection.AddItem(new Item("Item 3"));
            collection.AddItem(new Item("Item 4"));
            collection.AddItem(new Item("Item 5"));
            collection.AddItem(new Item("Item 6"));
            collection.AddItem(new Item("Item 7"));
            collection.AddItem(new Item("Item 8"));

            Iterator iterator = collection.CreateIterator();
            iterator.Step = 2;

            for (Item item = iterator.First();
                iterator.HasNext(); item = iterator.Next())
            {
                Console.WriteLine(item.Name);
            }
            Console.ReadKey();
        }
    }
}
