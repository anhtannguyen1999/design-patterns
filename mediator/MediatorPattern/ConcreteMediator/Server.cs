﻿using MediatorPattern.Colleague;
using MediatorPattern.Mediator;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorPattern.ConcreteMediator
{
    public class Server : IServer
    {
        List<ClientColleague> ListClients = new List<ClientColleague>();
        public void AddClient(ClientColleague client)
        {
            ListClients.Add(client);
        }

        public void ReceiveMsg(ClientColleague client, string Msg, List<int> ListId)
        {
            SendMsg(client, Msg, ListId);
        }

        public void SendMsg(ClientColleague sender, string Msg, List<int> ListId)
        {
            if (ListId != null)
            {
                ListClients.ForEach(cl =>
                {
                    if (ListId.Contains(cl.Id) && cl.Id != sender.Id)
                        cl.ReceiveMsg(sender, Msg);
                });
            }
            else
            {
                ListClients.ForEach(cl =>
                {
                    if (cl.Id != sender.Id)
                        cl.ReceiveMsg(sender, Msg);
                });
            }
        }
    }
}
