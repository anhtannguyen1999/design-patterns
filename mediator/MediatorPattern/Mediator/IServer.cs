﻿using MediatorPattern.Colleague;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorPattern.Mediator
{
    public interface IServer
    {
        public void ReceiveMsg(ClientColleague client, string Msg, List<int> ListId);
        public void SendMsg(ClientColleague client,string Msg, List<int> ListID);
        public void AddClient(ClientColleague client);
    }
}
