﻿using MediatorPattern.Colleague;
using MediatorPattern.ConcreteColleague;
using MediatorPattern.ConcreteMediator;
using MediatorPattern.Mediator;
using System;

namespace MediatorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            IServer s = new Server();
            ClientColleague client1 = new Client() { Id = 1, Name = "Client 1" };
            ClientColleague client2 = new Client() { Id = 2, Name = "Client 2" };
            ClientColleague client3 = new Client() { Id = 3, Name = "Client 3" };
            ClientColleague client4 = new Client() { Id = 4, Name = "Client 4" };
            
            s.AddClient(client2);
            s.AddClient(client3);
            s.AddClient(client4);
            client1.SendMsg(s, "Hello", null);
            Console.ReadKey();
        }
    }
}
