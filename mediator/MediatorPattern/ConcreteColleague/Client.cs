﻿using MediatorPattern.Colleague;
using MediatorPattern.Mediator;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorPattern.ConcreteColleague
{
    public class Client: ClientColleague
    {       
        public override  void SendMsg(IServer server, string Msg, List<int> ListId) {
            server.ReceiveMsg(this, Msg, ListId);
        }
        public override void ReceiveMsg(ClientColleague client,string Msg) {
            Console.WriteLine("Client [" + Name + "] receive msg [" + Msg + "] from Client [" + client.Name + "]");
        }
    }
}
