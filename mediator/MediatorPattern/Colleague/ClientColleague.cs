﻿using MediatorPattern.Mediator;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorPattern.Colleague
{
    public abstract class ClientColleague
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual void SendMsg(IServer server, string Msg,List<int> ListId) { }
        public virtual void ReceiveMsg(ClientColleague client,string Msg) { }
    }
}
