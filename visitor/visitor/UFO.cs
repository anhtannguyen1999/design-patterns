﻿using System;
using System.Collections.Generic;
using System.Text;

namespace visitor
{
    public class UFO : Enemy
    {
        public void hitBy(Warrior warrior)
        {
            damaged(50);
        }

        public void hitBy(Wizard wizard)
        {
            damaged(10);
        }

        private void damaged(int hp)
        {
            Console.WriteLine("UFO! I lost " + hp + "hp");
        }
    }
}
