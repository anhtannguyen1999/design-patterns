﻿using System;
using System.Collections.Generic;
using System.Text;

namespace visitor
{
    public class Dracula : Enemy
    {
        public void hitBy(Warrior warrior)
        {
            damaged(15);
        }

        public void hitBy(Wizard wizard)
        {
            damaged(80);
        }

        private void damaged(int hp)
        {
            Console.WriteLine("Dracula! I lost " + hp + "hp");
        }
    }
}
