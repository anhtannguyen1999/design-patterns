﻿using System;
using System.Collections.Generic;
using System.Text;

namespace visitor
{
    public interface Enemy
    {
        void hitBy(Warrior warrior);
        void hitBy(Wizard wizard);
    }
}
