﻿using System;

namespace visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Human warrior = new Warrior();
            Human wizard = new Wizard();

            Enemy ufo = new UFO();
            Enemy dracula = new Dracula();

            warrior.hit(ufo);
            wizard.hit(ufo);

            warrior.hit(dracula);
            wizard.hit(dracula);
        }
    }
}
