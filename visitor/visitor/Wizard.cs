﻿using System;
using System.Collections.Generic;
using System.Text;

namespace visitor
{
    public class Wizard : Human
    {
        public void hit(Enemy enemy)
        {
            Console.WriteLine("Wizard hit the enemy!");
            enemy.hitBy(this);
        }
    }
}
