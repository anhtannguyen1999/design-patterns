﻿using System;
using System.Collections.Generic;
using System.Text;

namespace visitor
{
    public class Warrior : Human
    {
        public void hit(Enemy enemy)
        {
            Console.WriteLine("Warrior hit the enemy!");
            enemy.hitBy(this);
        }
    }
}
