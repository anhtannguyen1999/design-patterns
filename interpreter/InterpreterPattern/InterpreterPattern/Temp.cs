﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterpreterPattern
{
    public class Main
    {
        public int Interpret(string Input)
        {
            if (Input.Length == 0)
                return 0;
            if (Input.StartsWith("IX"))
                return 9;
            else if (Input.StartsWith("IV"))
                return 4;
            else if (Input.StartsWith("V"))
                return 5;
            else if(Input.StartsWith("I"))
                return 1;
            return 0;
        }
    }

}
