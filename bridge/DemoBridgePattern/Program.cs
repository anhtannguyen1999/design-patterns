﻿using DemoBridgePattern.Asbtraction;
using DemoBridgePattern.ConcreteImplementor;
using DemoBridgePattern.RefinedAbstraction;
using System;

namespace DemoBridgePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Robot robotVN = new RobotVietNam()
            {
                MoveImplementor = new Run(),
                Name = "Robot 123",
                Velocity = 500
            };
            

            Robot robotCN = new RobotChina()
            {
                MoveImplementor = new Fly(),
                Name = "Robot 321",
                Velocity = 10
            };
            robotCN.FullInfor();

            Console.ReadKey();
        }
    }
}
