﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoBridgePattern.Implementor
{
    public interface Move
    {
        void Move(int Km);
    }
}
