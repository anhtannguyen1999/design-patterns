﻿using DemoBridgePattern.Implementor;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoBridgePattern.ConcreteImplementor
{
    public class Run : Move
    {
        public void Move(int Km)
        {
            Console.WriteLine("Run with " + Km);
        }
    }
}
