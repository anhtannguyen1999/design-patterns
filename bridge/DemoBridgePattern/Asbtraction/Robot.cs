﻿using DemoBridgePattern.Implementor;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoBridgePattern.Asbtraction
{
    public class Robot
    {
        public Move MoveImplementor { get; set; }
        public string Name { get; set; }
        public int Velocity { get; set; }

        public virtual void FullInfor()
        {
            Console.WriteLine("Robot's name is: "+Name);
            MoveImplementor.Move(Velocity);
        }
    }
}
