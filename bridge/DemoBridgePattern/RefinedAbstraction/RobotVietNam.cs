﻿using DemoBridgePattern.Asbtraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoBridgePattern.RefinedAbstraction
{
    public class RobotVietNam : Robot
    {
        public override void FullInfor()
        {
            Console.WriteLine("RobotVietNam's name is: " + Name);
            MoveImplementor.Move(Velocity);
        }
    }
}
