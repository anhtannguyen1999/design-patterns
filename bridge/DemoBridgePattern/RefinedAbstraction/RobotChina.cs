﻿using DemoBridgePattern.Asbtraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoBridgePattern.RefinedAbstraction
{
    public class RobotChina : Robot
    {
        public override void FullInfor()
        {
            Console.WriteLine("RobotChina's name is: " + Name);
            MoveImplementor.Move(Velocity);
        }

    }
}
