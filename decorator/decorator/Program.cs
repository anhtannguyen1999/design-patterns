﻿using System;

namespace decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            IGun AK47 = new AK47(); 
            Console.WriteLine("AK47: " + AK47.ShootingDamage()); // ShootingDame() = 30

            AK47 = new Muzzle3Decorator(AK47);
            Console.WriteLine("AK47+Muzzle3: " + AK47.ShootingDamage()); // ShootingDame() = 30 + 9 = 39

            AK47 = new Scope4Decorator(AK47); 
            Console.WriteLine("AK47+Muzzle3+Scope4: " + AK47.ShootingDamage());// ShootingDame() = 39 + 10 =49

            IGun MyScar = new Scar();
            MyScar = new Muzzle3Decorator(MyScar); //Dame = 25 + 9
            Console.WriteLine("Scar+Muzzle3: " + MyScar.ShootingDamage());

            Console.ReadKey();
        }
    }
}
