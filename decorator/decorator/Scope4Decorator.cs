﻿using System;
using System.Collections.Generic;
using System.Text;

namespace decorator
{
    public class Scope4Decorator : GunDecorator
    {
        public Scope4Decorator(IGun gun) : base(gun)
        {
        }

        public override int ShootingDamage()
        {
            //Console.WriteLine("scop");
            return this.gun.ShootingDamage() + Scope4IncreaseDame();
        }

        public int Scope4IncreaseDame()
        {
            return 10;
        }
    }
}
