﻿using System;
using System.Collections.Generic;
using System.Text;

namespace decorator
{
    public class Muzzle3Decorator : GunDecorator
    {
        public Muzzle3Decorator(IGun gun) : base(gun)
        {
        }

        public override int ShootingDamage()
        {
            //Console.WriteLine("muzzle");
            return this.gun.ShootingDamage() + Muzzle3IncreaseDame();
        }

        public int Muzzle3IncreaseDame()
        {
            return 9;
        }
    }
}
