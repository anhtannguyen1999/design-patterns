﻿using System;
using System.Collections.Generic;
using System.Text;

namespace decorator
{
    public interface IGun
    {
        int ShootingDamage();
    }
}
