﻿using System;
using System.Collections.Generic;
using System.Text;

namespace decorator
{
    public abstract class GunDecorator : IGun
    {
        protected IGun gun;

        public GunDecorator(IGun gun)
        {
            this.gun = gun;
        }

        public abstract int ShootingDamage();

    }
}
