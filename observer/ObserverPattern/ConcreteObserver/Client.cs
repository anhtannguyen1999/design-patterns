﻿using ObserverPattern.Observer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern.ConcreteObserver
{
    public class Client : IClient
    {
        public int IdClient { get; set; }
        public Client(int Id)
        {
            IdClient = Id;
        }
        public void ReceiveMsg(string msg)
        {
            Console.WriteLine("Client "+IdClient+" receive msg: "+msg);
        }
    }
}
