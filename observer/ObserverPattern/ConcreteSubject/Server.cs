﻿using ObserverPattern.ConcreteObserver;
using ObserverPattern.Observer;
using ObserverPattern.Subject;
using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern.ConcreteSubject
{
    public class Server : IServer
    {
        List<IClient> Clients = new List<IClient>();
        public void AddClient(IClient client)
        {
            Clients.Add(client);
        }

        public void RemoveClient(IClient client)
        {
            Clients.Remove(client);
        }

        public void SendMsg(string msg)
        {
            Clients.ForEach(cl =>
            {
                cl.ReceiveMsg(msg);
            });
        }
    }
}
