﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern.Observer
{
    public interface IClient
    {
        void ReceiveMsg(string msg);
    }
}
