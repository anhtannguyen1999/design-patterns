﻿using ObserverPattern.ConcreteObserver;
using ObserverPattern.ConcreteSubject;
using System;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Server s = new Server();

            s.AddClient(new Client(1));
            s.AddClient(new Client(2));
            s.AddClient(new Client(3));

            s.SendMsg("hello");
            


            Console.ReadKey();
        }
    }
}
