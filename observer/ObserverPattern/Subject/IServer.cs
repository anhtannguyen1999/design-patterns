﻿using ObserverPattern.Observer;
using System;
using System.Collections.Generic;
using System.Text;

namespace ObserverPattern.Subject
{
    public interface IServer
    {
        void AddClient(IClient client);
        void RemoveClient(IClient client);
        void SendMsg(string msg);

    }
}
