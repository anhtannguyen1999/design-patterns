﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.Src
{
    public class Sprite
    {
        public Image SpriteImage { get; set; }
        public void Draw(Graphics gfx,int X,int Y)
        {
            // Draw sprite image on screen
            gfx.DrawImage(SpriteImage, new RectangleF(X, Y, SpriteImage.Width, SpriteImage.Width));
        }
    }
}
