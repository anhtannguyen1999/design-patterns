﻿using StateDesignPattern.Client;
using StateDesignPattern.ConcreteState;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StateDesignPattern
{
    public partial class GameApp : Form
    {
        Timer graphicsTimer;
        GameLoop gameLoop;
        public GameApp()
        {

            InitializeComponent();
            // Initialize Paint Event
            Paint += GameApp_Paint;
            // Initialize graphicsTimer
            graphicsTimer = new Timer();
            graphicsTimer.Interval = 1000 / 120;
            graphicsTimer.Tick += GraphicsTimer_Tick;
        }
        private void GameApp_Load(object sender, EventArgs e)
        {
            Rectangle resolution = Screen.PrimaryScreen.Bounds;

            // Initialize Game
            //DemoStatePattern.Game.Game myGame = new DemoStatePattern.Game.Game();
            //myGame.Resolution = new Size(resolution.Width, resolution.Height);

            // Initialize & Start GameLoop
            gameLoop = new GameLoop();
            gameLoop.Load(Ninja.GetNinjaInstance());
            gameLoop.Start();

            // Start Graphics Timer
            graphicsTimer.Start();
        }
        private void GameApp_Paint(object sender, PaintEventArgs e)
        {
            // Draw game graphics on Form1
            gameLoop.Render(e.Graphics);
        }
        private void GraphicsTimer_Tick(object sender, EventArgs e)
        {
            // Refresh Form1 graphics
            Invalidate();
        }


        private void GameApp_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 's':
                    Ninja.GetNinjaInstance().SetState(new IdleState());
                    e.Handled = true;
                    break;
                case 'd':
                    Ninja.GetNinjaInstance().SetState(new RunRightState());
                    e.Handled = true;
                    break;
                case 'w':
                    Ninja.GetNinjaInstance().SetState(new JumpState());
                    e.Handled = true;
                    break;
            }

        }
    }

}
