﻿using StateDesignPattern.Client;
using StateDesignPattern.Src;
using StateDesignPattern.State;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.ConcreteState
{
    public class IdleState : StateNinja
    {
        Sprite Idle1 = new Sprite() { SpriteImage = Image.FromFile("./Src/Idle1.png") };
        public void Update()
        {
            Ninja.GetNinjaInstance().CurrentSprite = Idle1;
        } 
    }
}
