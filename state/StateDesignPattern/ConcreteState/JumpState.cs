﻿using StateDesignPattern.Client;
using StateDesignPattern.Src;
using StateDesignPattern.State;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.ConcreteState
{
    public class JumpState : StateNinja
    {
        Sprite Jump1 = new Sprite() { SpriteImage = Image.FromFile("./Src/Jump1.png") };
        Sprite Jump2 = new Sprite() { SpriteImage = Image.FromFile("./Src/Jump2.png") };
        Sprite Jump3 = new Sprite() { SpriteImage = Image.FromFile("./Src/Jump3.png") };
        Sprite Jump4 = new Sprite() { SpriteImage = Image.FromFile("./Src/Jump4.png") };

        int Index = 1;
        bool Up = true;
        public void Update()
        {
            switch (Index)
            {
                case 1:
                    Ninja.GetNinjaInstance().CurrentSprite = Jump1;
                    break;
                case 2:
                    Ninja.GetNinjaInstance().CurrentSprite = Jump2;
                    break;
                case 3:
                    Ninja.GetNinjaInstance().CurrentSprite = Jump3;
                    break;
                default:
                    Ninja.GetNinjaInstance().CurrentSprite = Jump4;
                    break;
            }
            if (Ninja.GetNinjaInstance().Y <= 55 && Up)
                Up = false;

            if (Up)
                Ninja.GetNinjaInstance().Y -= Ninja.GetNinjaInstance().Velocity;
            else
                if (Ninja.GetNinjaInstance().Y >= 100)
                    Ninja.GetNinjaInstance().SetState(new IdleState());
            else
                Ninja.GetNinjaInstance().Y += Ninja.GetNinjaInstance().Velocity;
            Index = Index % 4 + 1;

        }

    }
}
