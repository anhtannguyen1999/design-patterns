﻿using StateDesignPattern.Client;
using StateDesignPattern.Src;
using StateDesignPattern.State;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.ConcreteState
{
    public class RunRightState : StateNinja
    {
        Sprite Run1 = new Sprite() { SpriteImage = Image.FromFile("./Src/Run1.png") };
        Sprite Run2 = new Sprite() { SpriteImage = Image.FromFile("./Src/Run2.png") };
        Sprite Run3 = new Sprite() { SpriteImage = Image.FromFile("./Src/Run3.png") };

        int Index = 1;
        public void Update()
        {
            switch(Index)
            {
                case 1:
                    Ninja.GetNinjaInstance().CurrentSprite = Run1;
                    break;
                case 2:
                    Ninja.GetNinjaInstance().CurrentSprite = Run2;
                    break;
                default:
                    Ninja.GetNinjaInstance().CurrentSprite = Run3;
                    break;
            }
            Ninja.GetNinjaInstance().X += Ninja.GetNinjaInstance().Velocity;
            Index = Index % 3+1;

        }
    }
}
