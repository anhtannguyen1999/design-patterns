﻿using StateDesignPattern.ConcreteState;
using StateDesignPattern.Src;
using StateDesignPattern.State;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.Client
{
    public class Ninja
    {
        private static Ninja ninjaInstance;
        public int X { get; set; }
        public int Y { get; set; }
        public int Velocity { get; set; }
        public Sprite CurrentSprite { get; set; }
        public StateNinja state { get; set; }
        public Ninja()
        {
            X = 100;
            Y = 100;
            Velocity = 15;
            state = new IdleState();

        }
        public static Ninja GetNinjaInstance()
        {
            if (ninjaInstance == null)
                ninjaInstance = new Ninja();
            return ninjaInstance;
        }

        public void Update(TimeSpan gameTime)
        {
            state.Update();
        }
        public void SetState(StateNinja _state)
        {
            state = _state;
        }
        public void Render(Graphics gfx)
        {
            CurrentSprite.Draw(gfx, X, Y);
        }
        
    }
}
