﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StateDesignPattern.State
{
    public interface StateNinja
    {
        public void Update();
    }
}
