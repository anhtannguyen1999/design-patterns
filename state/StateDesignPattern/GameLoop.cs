﻿using StateDesignPattern.Client;
using StateDesignPattern.Src;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace StateDesignPattern
{
    public class GameLoop
    {
        private Ninja _myNinja;
        Sprite Map = new Sprite() { SpriteImage= Image.FromFile("./Src/Map.png") };
        public bool Running { get; private set; }

        public void Load(Ninja gameObj)
        {
            _myNinja = Ninja.GetNinjaInstance();
        }

        public async void Start()
        {

            Running = true;

            DateTime _previousGameTime = DateTime.Now;

            while (Running)
            {
                // Calculate the time elapsed since the last game loop cycle
                TimeSpan GameTime = DateTime.Now - _previousGameTime;
                // Update the current previous game time
                _previousGameTime = _previousGameTime + GameTime;
                // Update the game
                _myNinja.Update(GameTime);
                // Update Game at 60fps
                await Task.Delay(100);
            }
        }

        public void Render(Graphics gfx)
        {
            //Map.Draw(gfx, 0, -200);
            Font drawFont = new Font("Arial", 16);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            gfx.DrawString(Ninja.GetNinjaInstance().state.GetType().ToString(), drawFont, drawBrush, 0, 0);
            string pos = "X="+Ninja.GetNinjaInstance().X.ToString()+" Y=" +Ninja.GetNinjaInstance().Y.ToString();
            gfx.DrawString(pos, drawFont, drawBrush, 0, 25);
            _myNinja.Render(gfx);
            
        }
    }
}
