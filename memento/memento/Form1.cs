﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace memento
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            calculator.SetResult(0);
            careTaker.Add(calculator.CreateMemento());
        }

        Calculator calculator = new Calculator();
        CareTaker careTaker = new CareTaker();
        

        private int? x = null;
        private int? y = null;
        private string phepTinh = "NON";
        private void btn1_Click(object sender, EventArgs e)
        {
            int num = Int32.Parse(((Button)sender).Text);
            tbx.Text = tbx.Text + num.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int num = Int32.Parse(tbx.Text);
            x = num;
            phepTinh = "ADD";
            tbx.Text = "0";
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            int num = Int32.Parse(tbx.Text);
            x = num;
            phepTinh = "SUB";
            tbx.Text = "0";
        }

        private void btnEQ_Click(object sender, EventArgs e)
        {
            int num = Int32.Parse(tbx.Text);
            y = num;
            int c = 0; 
            if (phepTinh == "ADD")
            {
                c= (int)x + (int)y;
            }
            else if (phepTinh == "SUB")
            {
                c = (int)x - (int)y;
            }
            else
            {
                return;
            }
            calculator.SetResult(c);
            careTaker.Add(calculator.CreateMemento());
            tbx.Text = c.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            x = null;
            y = null;
            tbx.Text = "0";
            phepTinh = "NON";
        }

        private void btnPre_Click(object sender, EventArgs e)
        {
            careTaker.Get();
            Memento pre = careTaker.Get();
            calculator.SetResult(pre.GetState());
            tbx.Text = pre.GetState().ToString();
        }
    }
}
