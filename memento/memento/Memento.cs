﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace memento
{
    public class Memento
    {
        int state;
        public int GetState()
        {
            return state;
        }
        public void SetState(int state)
        {
            this.state = state;
        }
    }
}
