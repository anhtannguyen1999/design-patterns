﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace memento
{
    public class Calculator
    {
        int result;
        public Calculator(int i = 0)
        {
            result = 0;
        }
        public void SetResult(int i = 0)
        {
            this.result = i;
        }
        public void Add(int x)
        {
            result += x;
        }
        public void Subtract(int x)
        {
            result -= x;
        }
        public int GetResult()
        {
            return result;
        }
        public Memento CreateMemento()
        {
            Memento memento = new Memento();
            memento.SetState(result);
            return memento;
        }
        public void SetMemento(Memento memento)
        {
            result = memento.GetState();
        }
    }
}
