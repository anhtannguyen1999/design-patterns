﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace memento
{
    public class CareTaker
    {
        private Stack<Memento> saveList = new Stack<Memento>();

        public void Add(Memento state)
        {
            saveList.Push(state);
        }

        public Memento Get()
        {
            return saveList.Count()==0 ? null : saveList.Pop();
        }
    }
}
