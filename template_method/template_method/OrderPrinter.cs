﻿using System;
using System.Collections.Generic;
using System.Text;

namespace template_method
{
    public abstract class OrderPrinter
    {
        public void PrintOrder(Order order)
        {
            Format();
            Header();
            UserInfor(order);
            Detail(order);
            Footer(order);
        }

        protected abstract void Format();
        protected abstract void Header();
        protected abstract void UserInfor(Order order);
        protected abstract void Detail(Order order);
        protected abstract void Footer(Order order);

    }
}
