﻿using System;
using System.Collections.Generic;
using System.Text;

namespace template_method
{
    public class TextPrinter : OrderPrinter
    {
        protected override void Detail(Order order)
        {
            Console.WriteLine("----------------------");
            foreach (var item in order.Items)
            {
                Console.Write(item.Key + " : ");
                Console.WriteLine(item.Value + " VND");
            }
            Console.WriteLine("--- Total: " + order.Total+" ---");
        }

        protected override void Footer(Order order)
        {
            Console.WriteLine("----------------------");
            Console.WriteLine("HCM, " + order.Date);
            Console.WriteLine("Cam on quy khach!");
        }

        protected override void Format()
        {
            
        }

        protected override void Header()
        {
            Console.WriteLine("------------------------");
            Console.WriteLine("---HOA DON THANH TOAN---");
            Console.WriteLine("------Sweet--Coffe------");
            
        }

        protected override void UserInfor(Order order)
        {
            Console.WriteLine("KH: "+ order.CustomerName);
        }
    }
}
