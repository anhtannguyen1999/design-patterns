﻿using System;
using System.Collections.Generic;
using System.Text;

namespace template_method
{
    public class Order
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public string CustomerName { get; set; }
        public Dictionary<string, UInt64> Items { get; set; }
        public UInt64 Total { get; set; }
    }
}
