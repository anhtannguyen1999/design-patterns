﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace template_method
{
    public class HtmlPrinter : OrderPrinter
    {
        protected override void Detail(Order order)
        {
            string headtable = "<table class=\"TableData\"> <tr> <th>STT</th> <th>Tên</th> <th>Đơn giá</th> <th>Số</th> <th>Thành tiền</th> </tr>";
            writeToFileWithUTF8("invoice.html", headtable);
            int i = 0;
            foreach (var item in order.Items)
            {
                i++;
                string detail = String.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",i,item.Key,item.Value,1,item.Value);
                writeToFileWithUTF8("invoice.html", detail);
            }
            string total = String.Format("<tr> <td colspan=\"4\" class=\"tong\">Tổng cộng</td> <td class=\"cotSo\">{0}</td> </tr> </table>", order.Total);
            writeToFileWithUTF8("invoice.html", total);
        }

        protected override void Footer(Order order)
        {
            string leftFooter = "<div class=\"footer-left\"> HCM, "+order.Date+"<br/> Khách hàng </div>";
            string rightFooter = "<div class=\"footer-right\"> HCM, " + order.Date+ "<br/> Nhân viên </div>";
            string end = "</div></body>";
            writeToFileWithUTF8("invoice.html", leftFooter);
            writeToFileWithUTF8("invoice.html", rightFooter);
            writeToFileWithUTF8("invoice.html", end);
        }

        protected override void Format()
        {
            if (File.Exists("invoice.html"))
            {
                File.Delete("invoice.html");
            }
            string content = "<head><style>.page{ width: 21cm; overflow:hidden; min-height:297mm; padding: 2.5cm; margin-left:auto; margin-right:auto; background: white; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); } .header { overflow:hidden; } .company { padding-top:24px; text-transform:uppercase; background-color:#FFFFFF; text-align:right; float:right; font-size:16px; } .title { text-align:center; position:relative; color:#0000FF; font-size: 24px; top:1px; } .footer-left { text-align:center; text-transform:uppercase; padding-top:24px; position:relative; height: 150px; width:50%; color:#000; float:left; font-size: 12px; bottom:1px; } .footer-right { text-align:center; text-transform:uppercase; padding-top:24px; position:relative; height: 150px; width:50%; color:#000; font-size: 12px; float:right; bottom:1px; } .TableData { background:#ffffff; font: 11px; width:100%; border-collapse:collapse; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; border:thin solid #d3d3d3; } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }</style></head>";
            writeToFileWithUTF8("invoice.html", content);
        }

        protected override void Header()
        {
            string content = "<body> <div id=\"page\" class=\"page\"> <div class=\"header\"> <div class=\"company\">--Sweet-Coffee--</div> </div> <br/> <div class=\"title\"> HÓA ĐƠN THANH TOÁN <br/> -------oOo------- </div>";
            writeToFileWithUTF8("invoice.html", content);
        }

        protected override void UserInfor(Order order)
        {
            string content = "<br/> <p>Khách hàng: "+order.CustomerName+"</p>";
            writeToFileWithUTF8("invoice.html", content);
        }

        private void writeToFileWithUTF8(string filePath, string txt)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Append))
            {
                using (StreamWriter writer = new StreamWriter(fs, Encoding.UTF8))
                {
                    writer.WriteLine(txt);
                }
            }
        }
    }
}
