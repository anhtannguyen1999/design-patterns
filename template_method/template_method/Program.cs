﻿using System;
using System.Collections.Generic;

namespace template_method
{
    class Program
    {
        static void Main(string[] args)
        {
            Order order = new Order { 
                ID = 1, 
                CustomerName = "Nguyen Anh Tan", 
                Date = "01/10/2020", 
                Items = new Dictionary<string, UInt64>() {
                    { "Cafe da", 15000},
                    { "Tra dao cam xa", 20000},
                    {"Yaout dau", 25000 }
                },
                Total = 60000
            };

            OrderPrinter textPrinter = new TextPrinter();
            textPrinter.PrintOrder(order);

            OrderPrinter htmlPrinter = new HtmlPrinter();
            htmlPrinter.PrintOrder(order);
        }
    }
}
