﻿using ProxyPattern.RealSubject;
using ProxyPattern.Subject;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Proxy
{
    public class ImageProxy : Image
    {
        private RealImage realImage;
        private string Path;
        public ImageProxy(string _path)
        {
            Path = _path;
        }
        public void ShowImage()
        {
            if (realImage == null)
            {
                realImage = new RealImage(Path);
                Console.WriteLine("First init Image id: " + realImage.Id);
            }
            realImage.ShowImage();
                
        }
    }
}
