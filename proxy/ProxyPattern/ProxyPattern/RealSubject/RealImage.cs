﻿using ProxyPattern.Subject;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.RealSubject
{
    public class RealImage : Image
    {
        public int Id;
        private string Path;
        public RealImage(string _path)
        {
            Random r = new Random();
            Id = r.Next(1,500);
            Path = _path;
        }
        public void ShowImage()
        {
            Console.WriteLine("Image Id " + Id.ToString() +" Path: " + Path);
        }
    }
}
