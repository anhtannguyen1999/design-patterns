﻿using ProxyPattern.Proxy;
using ProxyPattern.Subject;
using System;

namespace ProxyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ImageProxy imageProxy = new ImageProxy("src/image1234.png");
            Image imageProxy2 = new ImageProxy("src/9999.png");
            imageProxy.ShowImage();
            imageProxy.ShowImage();
            Console.ReadKey();
        }
    }
}
