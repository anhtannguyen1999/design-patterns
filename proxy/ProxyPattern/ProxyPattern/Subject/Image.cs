﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Subject
{
    public interface Image
    {
        void ShowImage();
    }
}
