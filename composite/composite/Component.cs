﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompositeExam
{
    public interface IComponent
    {
        void DisplayPrice();
    }
}
