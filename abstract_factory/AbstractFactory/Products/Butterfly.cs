﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Products
{
    public class Butterfly : Animal
    {
        public string Speak()
        {
            return "Can not speak...";
        }
    }
}
