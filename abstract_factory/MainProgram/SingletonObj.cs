﻿using System;
using System.Threading;

namespace SingletonDemo
{
    public class SingletonObj
    {
        private static SingletonObj Instance;
        private static readonly object PadLock = new object();
        //private static SingletonObj Instance = new SingletonObj();//khoi tao theo kieu Eager

        public double Id { get; set; }
        private SingletonObj()
        {
            Random r = new Random();
            Id= r.Next(0,1000);
        }
        public void IncreaseID()
        {
            Id++;
        }

        //cac method lay ra instance cua class
        public static SingletonObj GetInstance_Eager()
        {
            return Instance;
        }

        public static SingletonObj GetInstance_LazyLoad()
        {
            if (Instance == null)
            {
                //Thread.Sleep(5000);
                Instance = new SingletonObj();
            }
                
            return Instance;
        }

        public static SingletonObj GetInstance_ThreadSafe()
        {
            lock (PadLock)
            {
                if (Instance == null)
                    Instance = new SingletonObj();
                return Instance;
            }
        }

        public static SingletonObj GetInstance_ThreadSafeDoubleCheck()
        {

            if (Instance == null)
            {
                lock (PadLock)
                {
                    if (Instance == null)
                        Instance = new SingletonObj();
                }
            }
            return Instance;

        }
    }
}
