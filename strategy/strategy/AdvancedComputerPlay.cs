﻿using System;
using System.Collections.Generic;
using System.Text;

namespace strategy
{
    public class AdvancedComputerPlay : IComputerPlayStrategy
    {
        void IComputerPlayStrategy.MakeMove()
        {
            Console.WriteLine("Computer made an Advanced move.");
        }
    }
}
