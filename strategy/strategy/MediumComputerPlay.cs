﻿using System;
using System.Collections.Generic;
using System.Text;

namespace strategy
{
    public class MediumComputerPlay: IComputerPlayStrategy
    {
        void IComputerPlayStrategy.MakeMove()
        {
            Console.WriteLine("Computer made an Medium move.");
        }
    }
}
