﻿using System;
using System.Collections.Generic;
using System.Text;

namespace strategy
{
    public class ChessGame
    {
        private IComputerPlayStrategy computerPlayStrategy;
        public IComputerPlayStrategy ComputerPlayStrategy
        {
            get { return computerPlayStrategy; }
            set { computerPlayStrategy = value; }
        }
        public void Move()
        {
            computerPlayStrategy.MakeMove();   //exhibit the behavior
        }
    }
}
