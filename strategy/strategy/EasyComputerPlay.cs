﻿using System;
using System.Collections.Generic;
using System.Text;

namespace strategy
{
    public class EasyComputerPlay: IComputerPlayStrategy
    {
        void IComputerPlayStrategy.MakeMove()
        {
            Console.WriteLine("Computer made an Easy move.");
        }
    }
}
