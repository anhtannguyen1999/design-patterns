﻿using System;
using System.Collections.Generic;
using System.Text;

namespace strategy
{
    public interface IComputerPlayStrategy
    {
        void MakeMove();
    }
}
