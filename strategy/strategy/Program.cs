﻿using System;

namespace strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            ChessGame game = new ChessGame();

            game.ComputerPlayStrategy = new EasyComputerPlay();
            game.Move();   //move using Easy difficulty

            game.ComputerPlayStrategy = new MediumComputerPlay();
            game.Move();   //move using Medium difficulty

            game.ComputerPlayStrategy = new AdvancedComputerPlay();
            game.Move();   //move using Advanced difficulty
        }
    }
}
