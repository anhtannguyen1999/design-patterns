﻿using System;

namespace chain_of_responsibility
{
    class Program
    {
        public static DispenserHandler setChainDispensers()
        {
            DispenserHandler dispenser500k = new Dispenser500K(); 
            DispenserHandler dispenser200k = new Dispenser200k(); 
            DispenserHandler dispenser100k = new Dispenser100k();
            dispenser500k.SetNext(dispenser200k);
            dispenser200k.SetNext(dispenser100k);
            return dispenser500k;
        }
        static void Main(string[] args)
        {
            DispenserHandler dispenser = setChainDispensers();
            
            int soTien = 0;
            while(soTien%100!=0 || soTien < 100)
            {
                Console.WriteLine("Nhap so tien can rut: ");
                string res = Console.ReadLine();
                soTien = Int32.Parse(res);
            }
            Console.WriteLine("----------------");
            Console.WriteLine("Rut "+soTien+"K:");
            dispenser.dispense(soTien);
        }
    }
}
