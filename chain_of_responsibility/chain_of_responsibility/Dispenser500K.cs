﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chain_of_responsibility
{
    public class Dispenser500K : DispenserHandler
    {
        public override void dispense(int soTien)
        {
            if (soTien >= 500)
            {
                int num = soTien / 500;
                int conLai = soTien % 500;
                Console.WriteLine("Rut " + num + " to 500K");
                if (conLai != 0) 
                    this.successor.dispense(conLai);
            }
            else
            {
                this.successor.dispense(soTien);
            }
        }
    }
}
