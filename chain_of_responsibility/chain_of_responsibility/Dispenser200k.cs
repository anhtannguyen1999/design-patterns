﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chain_of_responsibility
{
    public class Dispenser200k: DispenserHandler
    {
        public override void dispense(int soTien)
        {
            if (soTien >= 200)
            {
                int num = soTien / 200;
                int conLai = soTien % 200;
                Console.WriteLine("Rut " + num + " to 200K");
                if (conLai != 0)
                    this.successor.dispense(conLai);
            }
            else
            {
                this.successor.dispense(soTien);
            }
        }
    }
}
