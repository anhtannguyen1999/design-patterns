﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chain_of_responsibility
{
    public abstract class DispenserHandler
    {
        protected DispenserHandler successor;
        public void SetNext(DispenserHandler next)
        {
            successor = next;
        }
        public abstract void dispense(int soTien);
    }
}
