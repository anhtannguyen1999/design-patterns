﻿using System;
using System.Collections.Generic;
using System.Text;

namespace chain_of_responsibility
{
    public class Dispenser100k: DispenserHandler
    {
        public override void dispense(int soTien)
        {
            if (soTien >= 100)
            {
                int num = soTien / 100;
                int conLai = soTien % 100;
                Console.WriteLine("Rut " + num + " to 100K");
                if (conLai != 0)
                    this.successor.dispense(conLai);
            }
            else
            {
                this.successor.dispense(soTien);
            }
        }
    }
}
